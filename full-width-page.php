<?php
/*
	Template Name: Fullwidth Page
*/

/**
 *	Pervotsvet Theme
 *
 *	osom themes
 *	osom.top
 */

the_post();

get_header();

echo '<div class="page-container standalone-container">';

the_content();

echo '</div>';

get_footer();
