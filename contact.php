<?php
/*
	Template Name: Contact Us
*/

/**
 *	Pervotsvet Theme
 *
 *	osom themes
 *	osom.top
 */

wp_enqueue_script('aurum-contact');

get_header();

get_template_part('tpls/contact');

get_footer();
