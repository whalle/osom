<?php
/**
 *	Pervotsvet Theme
 *
 *	osom themes
 *	osom.top
 */
get_header();

if(have_posts())
{
	the_post();
	the_content();
}

get_footer();
